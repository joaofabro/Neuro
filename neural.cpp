//# NEURO - BackPropagation Neural Networks
/*####################################################################################
Copyright 2018 Joao Fabro - joaofabro at gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
###################################################################################*/

// Program Neuro.cpp - main program of the neuro project - BackPropagation Neural Nets
// Joao Alberto Fabro - fabro at utfpr.edu.br - 05/2018


#include "neuro.h"

main(int argc, char *argv[])
{
  int ch;
  int i;
  int numneurlayers[NUMMAXNEURLAYER], numlayers;
  int numberofinputs,numberofoutputs;   
      
  double momentum=0.0;
  double learning_rate=0.1;
  double precision;
  vectorpair Pares;
  neural_net NEURO_NET;
  double ok; 
  long count;
  char extension[25];

  char line[30];
  FILE *fileConf;
  char fileName[30];
  int DEBUG=1;

  if(DEBUG) printf("Argc= %d\n", argc);

  if( (argc>1)&&(argc<=3) )
  {
   strcpy (fileName, FILE_CONFIG); 
   strcat (fileName, argv[1]);
   printf("Configuration file: %s\n", fileName);
   if ( (fileConf = fopen ( fileName, "r" )) == NULL )
    {
      cout << "Error reading configuration file " << fileName <<"!\n";
      exit(1);
    }

   if(fgets(line, 30, fileConf) == NULL)
      cout << "Error!";
   line[strlen(line)-1] = '\0';
   numberofinputs = atoi (line); 
   if(DEBUG) printf(" Number of Inputs : %d\n", numberofinputs);
   if(fgets(line, 30, fileConf) == NULL)
        cout << "Error!";
   line[strlen(line)-1] = '\0';
   numberofoutputs = atoi (line); 
   if (DEBUG) printf(" Number of Outputs : %d\n", numberofoutputs);
   if(fgets(line, 30, fileConf) == NULL)
        cout << "Error!";
   line[strlen(line)-1] = '\0';
   numlayers = atoi (line); 
   if (DEBUG) 
    printf(" Number of Layers of the Net: %d\n", numlayers);
   for(int j=0;j<numlayers;j++)
   {
     if(fgets(line, 30, fileConf) == NULL)
         cout << "Error!";
     line[strlen(line)-1] = '\0';    
     numneurlayers[j] = atoi (line); 
   if (DEBUG) 
    printf(" Number of Neurons on Layer %d : %d\n", j, numneurlayers[j]);
   }
   if(fgets(line, 30, fileConf) == NULL)
       cout << "Error!";
   line[strlen(line)-1] = '\0';
   momentum = atof (line); 
   if (DEBUG) 
    printf(" Momentum: %f\n", momentum);
   if(fgets(line, 30, fileConf) == NULL)
       cout << "Error!";
   line[strlen(line)-1] = '\0';
   learning_rate = atof (line); 
   if (DEBUG) 
    printf(" Learning Rate: %f\n", learning_rate);
   if(fgets(line, 30, fileConf) == NULL)
       cout << "Error!";
   line[strlen(line)-1] = '\0';
   precision = atof (line); 
   if (DEBUG) 
    printf(" Precision: %f\n", precision);
   
   fclose(fileConf); 
   NEURO_NET.create_net(numlayers, numneurlayers, numberofinputs);
   //,numberofdelayedinputs, numberofdelayedoutputs);
   NEURO_NET.set_parameters(momentum,learning_rate,precision); 

   if(argc==3)//In this case, the second parameter is the test config file!
   {
       NEURO_NET.load_net(argv[1]);
       Pares.load_vector(argv[2]); 
       NEURO_NET.remember(Pares); 
   }
   else
   {
    Pares.load_vector(argv[1]); 

    printf("Begin of training\n");
    count=0;
    do 
     { 
     ok = NEURO_NET.train_sequence(Pares); 
     cout << "Epoch " << count << "\t Error: " << ok;
     count++;
/*   if(count%1000 == 0)
   {       learning_rate +=0.01;
	   NEURO_NET.set_parameters(momentum,learning_rate,precision); 
   }*/
     }while(ok>0); 
    printf("End of training: %ld epochs\n", count);
   NEURO_NET.save_net(argv[1]); 
  }
 }
 else
 {
  precision = 0.01;
  strcpy(extension,"xor");
    Pares.load_vector(extension);

  cout << "(C)reate or (T)est :";
  ch = getchar();

  if ( (ch == 'C') || (ch == 'c') )
    {
      cout << "Input the number of Levels(including input) :";
      if(scanf ("%d", &numlayers)==0)
        cout << "ERROR";
      cout << "Input the number of inputs(size of the input part of the training pairs) :";
      if(scanf ("%d", &numberofinputs)==0)
        cout << "ERROR";
      for (i = 0; i < numlayers; i++)
	{
          cout << "Number of neurons on the hidden layer " << i << " :";
          if(scanf ("%d", &numneurlayers[i])==0)
            cout << "ERROR";
	}
      cout << "Precision of Training :";
      if(scanf ("%lf", &precision)==0)
        cout << "ERROR";
      NEURO_NET.create_net(numlayers, numneurlayers, numberofinputs);
      //,numberofdelayedinputs, numberofdelayedoutputs);
    }
  else
    NEURO_NET.load_net(extension);

  NEURO_NET.set_parameters(momentum,learning_rate,precision); 
  count=0;
  if ( (ch == 'c') || (ch == 'C') )
    do
    {
    ok = NEURO_NET.train_sequence(Pares);
    cout << "Epoch " << count << "\t Error: " << ok <<"\n";
    count++;
 //   getchar();
    }while(ok>0);
   printf("Got here!\n");
   if ( (ch == 't') || (ch == 'T') )
      NEURO_NET.remember(Pares);
  NEURO_NET.save_net(extension);
}// end of else!

  return(0);
}


