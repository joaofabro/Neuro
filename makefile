CC=g++
cc=gcc
CFLAGS=
OBJECTS=neuron.o neuronet.o trnpair.o vectorpair.o neural.o
LIBS= -L/usr/local/lib -lm 
INCLUDES= -I /usr/local/include/g++-3 -O3

neural:	neuron.o neuronet.o trnpair.o vectorpair.o neural.o 
	$(CC) neuron.o neuronet.o trnpair.o vectorpair.o neural.o -L/usr/local/lib -o neural -lm

neuron.o: neuron.cpp neuro.h
	$(CC) $(CFLAGS) -c neuron.cpp $(INCLUDES) -o neuron.o

neuronet.o: neuronet.cpp neuro.h
	$(CC) $(CFLAGS) -c neuronet.cpp $(INCLUDES) -o neuronet.o

trnpair.o: trnpair.cpp neuro.h
	$(CC) $(CFLAGS) -c trnpair.cpp $(INCLUDES) -o trnpair.o

vectorpair.o:  vectorpair.cpp neuro.h
	$(CC) $(CFLAGS) -c vectorpair.cpp $(INCLUDES) -o vectorpair.o

neural.o: neural.cpp neuro.h
	$(CC) $(CFLAGS) -c neural.cpp $(INCLUDES) -o neural.o

clean:
	rm *.o
	rm neural 
